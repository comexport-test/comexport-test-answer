package com.example.demo.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.demo.component.QuestionComponent;
import com.example.demo.component.UserComponent;
import com.example.demo.dto.AnswerDto;
import com.example.demo.dto.AnswerFilterDto;
import com.example.demo.dto.AnswerPersistDto;
import com.example.demo.exception.ApiException;
import com.example.demo.exception.ApiExceptionDomain.ExceptionType;
import com.example.demo.model.Answer;
import com.example.demo.repository.AnswerRepository;

@Service
public class AnswerService {

	@Autowired
	private AnswerRepository answerRepository;
	
	@Autowired
	private UserComponent userComponent;
	
	@Autowired
	private QuestionComponent questionComponent;
	
	private ModelMapper modelMapper;

	@Autowired
	public AnswerService(ModelMapper modelMapper) {
		this.modelMapper = modelMapper;
	}

	public List<AnswerDto> find(AnswerFilterDto filter) {
		Answer answerExample = new Answer();
		answerExample.setComment(filter.getComment());
		answerExample.setIdQuestion(filter.getIdQuestion());
		answerExample.setIdUser(filter.getIdUser());

		List<AnswerDto> result = new ArrayList<AnswerDto>();

		int page = filter.getPage() != null ? filter.getPage() : 0;
		int size = filter.getSize() != null && filter.getSize() < 1000 ? filter.getSize() : 1000;
		Pageable basePage = PageRequest.of(page, size);
		
		ExampleMatcher customExampleMatcher = ExampleMatcher.matchingAll()
				.withMatcher("comment", ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase())
				.withMatcher("idUser", ExampleMatcher.GenericPropertyMatchers.exact())
				.withMatcher("idQuestion", ExampleMatcher.GenericPropertyMatchers.exact());

		Example<Answer> example = Example.of(answerExample, customExampleMatcher);
		Page<Answer> queryResult = answerRepository.findAll(example, basePage); 
		queryResult.map(answer -> result.add(convertToDto(answer)));
		
		return result;
	}
	
	public Optional<AnswerDto> find(Long id) {
		return answerRepository.findById(id).map(answer -> convertToDto(answer));
	}

	public Long create(@Valid AnswerPersistDto dto) throws ApiException {
		
		userComponent.getUser(dto.getIdUser());
		questionComponent.getQuestion(dto.getIdQuestion());
		
		Answer answer = new Answer();
		answer.setComment(dto.getComment());
		answer.setIdUser(dto.getIdUser());
		answer.setIdQuestion(dto.getIdQuestion());

		LocalDateTime now = LocalDateTime.now();
		answer.setUpdatedAt(now);
		answer.setCreatedAt(now);
		
		answerRepository.save(answer);
		return answer.getIdAnswer();
	}

	public void update(@Valid AnswerPersistDto dto, Long id) throws ApiException {
		
		Answer original = answerRepository.findById(id)
				.orElseThrow(() -> new ApiException(Answer.class.getSimpleName(), 
						id.toString(), ExceptionType.NOT_FOUND));
		
		userComponent.getUser(dto.getIdUser());
		questionComponent.getQuestion(dto.getIdQuestion());
		
		original.setIdUser(dto.getIdUser());
		original.setIdQuestion(dto.getIdQuestion());
		original.setComment(dto.getComment());
		original.setUpdatedAt(LocalDateTime.now());

		answerRepository.save(original);
	}
	
	public void remove(@Valid Long id) throws ApiException {
		
		//TODO delete answers
		
		Answer original = answerRepository.findById(id)
				.orElseThrow(() -> new ApiException(Answer.class.getSimpleName(), id.toString(), ExceptionType.NOT_FOUND));
		
		answerRepository.delete(original);
	}
	
	private AnswerDto convertToDto(Answer answer) {
		AnswerDto dto = modelMapper.map(answer, AnswerDto.class);
		return dto;
	}

}
