package com.example.demo.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.AnswerDto;
import com.example.demo.dto.AnswerFilterDto;
import com.example.demo.dto.AnswerPersistDto;
import com.example.demo.dto.ResultDto;
import com.example.demo.exception.ApiException;
import com.example.demo.service.AnswerService;

@RestController
@RequestMapping("/api/answer")
class AnswerController {

	private final Logger log = LoggerFactory.getLogger(AnswerController.class);

	@Autowired
	private AnswerService service;

	@GetMapping
	Collection<AnswerDto> answers(AnswerFilterDto filter) {
		return service.find(filter);
	}

	@GetMapping("/{id}")
	ResponseEntity<?> answer(@PathVariable Long id) {
		
		Optional<AnswerDto> result = service.find(id);
		return result.map(response -> ResponseEntity.ok().body(response)).orElse(ResponseEntity.notFound().build());
	}

	@PostMapping
	ResponseEntity<ResultDto> create(
			@Valid @RequestBody AnswerPersistDto dto
			) throws URISyntaxException, ApiException {

		log.info("Request to create answer: ", dto);
		Long idAnswer = service.create(dto);
		return ResponseEntity.created(new URI("/api/answer/" + idAnswer))
				.body(new ResultDto(idAnswer, HttpStatus.CREATED));
	}

	@PutMapping("/{id}")
	ResponseEntity<ResultDto> update(
			@PathVariable Long id, 
			@Valid @RequestBody AnswerPersistDto dto
			) throws ApiException {
		
		log.info("Request to update answer: ", dto);
		service.update(dto, id);
		return ResponseEntity.ok()
				.body(new ResultDto(id, HttpStatus.OK));
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> remove(@PathVariable Long id) throws ApiException {
		
		log.info("Request to delete answer: ", id);
		service.remove(id);
		return ResponseEntity.ok().body("Answer removed: " + id);
	}

}