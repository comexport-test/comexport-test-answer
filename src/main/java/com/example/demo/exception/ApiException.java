package com.example.demo.exception;

import com.example.demo.exception.ApiExceptionDomain.ExceptionType;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;


@RequiredArgsConstructor
@Getter
@ToString
public class ApiException extends Exception {

	private static final long serialVersionUID = 1L;
	
	@NonNull
	private String entidade;
	
	@NonNull
	private String id;
	
	@NonNull
	private ExceptionType tipo;
	
	private String mensagem;

	public ApiException(@NonNull String entidade, @NonNull String id, @NonNull ExceptionType tipo, String mensagem) {
		super();
		this.entidade = entidade;
		this.id = id;
		this.tipo = tipo;
		this.mensagem = mensagem;
	}
	
}
