package com.example.demo.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.example.demo.dto.QuestionDto;
import com.example.demo.exception.ApiException;
import com.example.demo.exception.ApiExceptionDomain.ExceptionType;

@Component
public class QuestionComponent {
	
	@Autowired
	RestTemplate restTemplate;
	
	@Value("http://localhost:8002/api/question")
	String questionURL;

	public QuestionDto getQuestion(Long id) throws ApiException {
		try{
			String finalUrl = questionURL + '/' + id;
			return  restTemplate.getForEntity(finalUrl, QuestionDto.class).getBody();
			
		} catch(Exception e) {
			throw new ApiException(QuestionDto.class.getSimpleName(), id.toString(),
					ExceptionType.NOT_FOUND);
		}
		
	}
}
