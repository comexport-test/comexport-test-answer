package com.example.demo.dto;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class UserDto {
	
	Long id;
	
	@NotNull
	String name;
	
	@NotNull
	String email;
	
}
