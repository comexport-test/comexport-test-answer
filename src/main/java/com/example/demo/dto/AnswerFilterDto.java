package com.example.demo.dto;

import lombok.Data;

@Data
public class AnswerFilterDto {
	Integer page;
	Integer size;
	Long idUser;
	Long idQuestion;
	String comment;
}
