package com.example.demo.dto;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class QuestionDto {
	
	Long id;
	@NotNull
	Long idUser;
	Boolean resolved;
}
