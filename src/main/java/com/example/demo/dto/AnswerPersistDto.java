package com.example.demo.dto;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class AnswerPersistDto {
	
	@NotNull
	Long idUser;
	
	@NotNull
	Long idQuestion;
	
	@NotNull
	String comment;
	
}
